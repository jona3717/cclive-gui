#include "cclivegui.h"
#include "./ui_cclivegui.h"
#include "QMessageBox"
#include "QProcess"

CcliveGUI::CcliveGUI(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::CcliveGUI)
{
    ui->setupUi(this);

    process = new QProcess(this);
    connect(process, &QProcess::readyReadStandardOutput, [&]()
    {
        auto data = process->readAllStandardOutput();
        ui->txtStatus->setText(data);
    });
    connect(process, &QProcess::readyReadStandardError, [&]()
    {
        auto data = process->readAllStandardError();
        ui->txtStatus->setText(data);
    });

    setWindowTitle("Cclive GUI");
    ui->txtUrl->setFocus();
    ui->btnDownload->setDisabled(1);
}

CcliveGUI::~CcliveGUI()
{
    delete ui;
}

void CcliveGUI::on_btnAppend_clicked()
{
    ui->txtStatus->setText("");
    auto url = ui->txtUrl->text();

    if (url.isEmpty()){
        QMessageBox::warning(this, tr("Error!"), tr("¡Debes ingresar una dirección!"));
    }
    else{

        ui->lstUrls->addItem(url);
        ui->txtUrl->setText("");
        ui->txtUrl->setFocus();
        ui->btnDownload->setEnabled(1);
    }
}

void CcliveGUI::on_btnDownload_clicked()
{
    if (ui->lstUrls->count() == 0){
        QMessageBox::warning(this, tr("Error!"), tr("¡No se agregó ninguna URL!"));
    }
    else{
        ui->btnDownload->setDisabled(1);

        const int count = ui->lstUrls->count();
        for (int i = 0; i < count; ++i) {
            listUrls << ui->lstUrls->item(i)->text();
        }

        process->setProgram("cclive");
        process->setArguments(listUrls);
        process->start();

        ui->txtUrl->setText("");
    }
}

void CcliveGUI::on_btnRemove_clicked()
{
    delete ui->lstUrls->currentItem();
}
