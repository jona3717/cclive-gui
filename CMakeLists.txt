cmake_minimum_required(VERSION 3.5)

project(cclive-gui LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt5 COMPONENTS Widgets REQUIRED)
#find_package(cclive REQUIRED)

add_executable(cclive-gui
  main.cpp
  cclivegui.cpp
  cclivegui.h
  cclivegui.ui
)

target_link_libraries(cclive-gui PRIVATE Qt5::Widgets)
