#!/bin/bash

mkdir build
cd build
cmake ../
make
mv cclive-gui ../
cd ..
rm -rf build
